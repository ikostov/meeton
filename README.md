# Meeton


## Goal
establish an informal playgroud for experimenting with different technologies and social networking.


## Roadmap
- NodeRed - intro [node-red]
- NodeRed - hands-on with a sensors/laptop/etc.
- Push data to the cloud/kafka/....
- Data Visualization with Grafana
- AI image recognition


## Todo:

- [ ] find a cool name for the group

### Suggestions
Ich fänd's gut, wenn die Termine sich im Kern um Digitalisierung, Cloud-Technologien und AI kümmern würden. Da haben wir derzeit nen riesen Know-How Mangel. Wenn wir den Fokus der Spiel-Sessions nicht einschränken, fächert sich das Thematisch wahrscheinlich sehr stark auf und die Gefahr wächst, dass wir dann doch wieder recht schnell Dinge machen, die bei R&S Stand der Technik sind.

- Sich treffen, rum spielen, lernen
  - Meet / Play : "**Meet 'n Play**" (klingt bischen nach einer Zocker-Runde :( )
  - Play / Learn : "**Plearn**" :)  
  - Play / Learn : "**Play 'n Learn**"
  - Meet / Hack : "**Meet 'n Hack**" (klingt nach einer Metzgerei :) )
  - Play / Lab : "**PlayLab**"
- Digitalisierung, rum spielen, lernen, Hacken
  - Digitalisierung, rum spielen: "**DigiPlay**"
  - Digitalisierung bei R&S: "**DigitizeRS**""  <- mein Favorit bislang




